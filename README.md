# micasacare


# Prerequisite
    * [node.js]
    * [Mongodb]
    * [npm]

### Tech
New Project uses following modules:
* [Express] - Fast, unopinionated, minimalist web framework for node.
* [graphql] - The JavaScript reference implementation for GraphQL, a query language for APIs created by Facebook.
* [graphql-server-express] - Used as express middleware for graphqlExpress schema.
* [graphql-tools] - Useful tools to create and manipulate GraphQL schemas
* [winston] - logger
* [mongoose] - Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment.

### Installation and start
 * npm install
 * npm start
