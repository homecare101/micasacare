var UserModel = require('../../models/userModel');
exports.usersResolver = {
      users: async (root,{}) => {
        let users = await UserModel.find({}).exec()
        return users
      }
  }
exports.usersMutationResolver = {
  createUser : async(root,{input}) => {
    let userData = new UserModel(input);
    await userData.save();
    let lastData = await UserModel.find({}).sort({_id:-1}).limit(1)
    return lastData[0];
  }
}
