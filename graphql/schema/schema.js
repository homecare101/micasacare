
var makeExecutableSchema = require('graphql-tools').makeExecutableSchema;
var usertypeDefs = require('../type/users').usertypeDefs;

var queryList = require('../type/queryList').queryList;
var mutationList = require('../type/mutationList').mutationList;
var usersResolver = require('../resolver/users').usersResolver;
var usersMutationResolver = require('../resolver/users').usersMutationResolver;


var typeDefs = [...usertypeDefs,...queryList,...mutationList];

var resolvers =
 {
   Query : {...usersResolver},
   Mutation : {...usersMutationResolver}
 }

exports.schema = makeExecutableSchema({
      typeDefs,
      resolvers
    })
