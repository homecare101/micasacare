exports.usertypeDefs = [`
      type users {
        _id : String,
        name : String,
        email: String
      }
      input user {
        name: String,
        email : String
      }
      schema {
        query: Query
        mutation: Mutation
      }
    `];
