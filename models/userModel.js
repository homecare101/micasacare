'use strict'

const mongoose = require('mongoose'),
  Schema = mongoose.Schema

let UserSchema = new Schema({
  name: {
    type: String,
    required: 'Please provide the username'
  },
  email: {
    type: String,
    default: ''
  },
})

module.exports = mongoose.model('Users', UserSchema)
