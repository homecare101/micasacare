'use strict'
const express = require('express')
const server = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const logger = require('winston')
const cors = require('cors')
const config = require('./config/database.js').MongoDBConfiguration;
var graphqlExpress =  require('graphql-server-express').graphqlExpress;
var graphiqlExpress = require('graphql-server-express').graphiqlExpress;
mongoose.Promise = global.Promise
mongoose.connect(config.db.uri, config.db.options, (err) => {
    // Log Error
    if (err) {
      console.error('Could not connect to MongoDB!')
      logger.error(err)
      process.exit(1);
      return;
    } else {
      logger.log('silly', 'connected')
      mongoose.set('debug', config.db.debug)
    }
  })

server.use(function(req,res,next){
    if (mongoose.Connection.STATES.connected === mongoose.connection.readyState){
        next();
    } else {
        console.log("error" ,"DB not ready");
        res.status(503).send({success:false, message: 'DB not ready' });
    }
});
server.use('*', cors());
const schema = require('./graphql/schema/schema').schema;
server.use('/graphql', bodyParser.json(), graphqlExpress({schema}))

const homePath = '/graphiql'

server.use(homePath, graphiqlExpress({
  endpointURL: '/graphql'
}))

server.use(bodyParser.urlencoded({extended: true, limit: '50mb'}))

server.use(bodyParser.json({limit: '50mb'}))

server.set('port', (process.env.PORT ||3030));
//const secureServer = https.createServer(httpsOptions, server)
server.listen(server.get('port'), function () {
  logger.log('info', 'JSON Server is running on PORT ', server.get('port'))
})

module.exports = server
